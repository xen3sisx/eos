[
  {
    "type": "text",
    "states": [
      {
        "status": "default",
        "how_to_use": "This is normal state of the element.",
        "code": [
          "<label class='d-block'><strong>Username</strong></label>",
          "<input type='text'>"
        ]
      },
      {
        "status": "focused",
        "how_to_use": "A focused state communicates when a user has highlighted an element using a keyboard or currently interacting with the field.",
        "code": [
          "<label class='d-block'><strong>Username</strong></label>",
          "<input type='text' autofocus>"
        ]
      },
      {
        "status":"disabled",
        "how_to_use": "This prevents the user from interacting with a field until certain conditions are met. When possible, show an alert or error message to inform users why the element is disabled.",
        "code": [
          "<label class='d-block disabled'><strong>Username</strong></label>",
          "<input type='text' disabled>"
        ]
      },
      {
        "status": "error",
        "how_to_use": "Indicates there was an error with the field on submission. Only affected fields should be highlighted in red.",
        "code": [
          "<label class='d-block'><strong>Username</strong></label>",
          "<input type='text' class='error'>",
          "<div class='error-msg'>Username is required.</div>"
        ]
      },
      {
        "status": "approved-field",
        "how_to_use": "Indicates that required field has a valid formate and content.",
        "code": [
          "<label class='d-block'><strong>Username</strong></label>",
          "<input type='text' class='approved'>"
        ]
      },
      {
        "status":"optional",
        "how_to_use": "Use (optional) to indicate non-mandatory fields rather than indicating only mandatory fields. This practice has proved to reduce stress in users when filling out long forms.",
        "code": [
          "<label class='d-block'><strong>Username</strong><span class='optional-label'> (optional)</span></label>",
          "<input type='text'>"
        ]
      }
    ]
  },
  {
    "type": "password",
    "states": [
      {
        "status":"default",
        "how_to_use": "This is normal state of the element.",
        "code": [
          "<label class='d-block'><strong>Password</strong></label>",
          "<input type='password'>",
          "<i class='eos-icons eos-18 toggle-password js-toggle-password'>visibility</i>",
          "<div class='password-strength-meter'><div class='meter'></div><div class='meter'></div><div class='meter'></div><div class='meter-text'></div></div>",
          "<div class='password-criteria-text'>Your password must be 8-20 characters long, contain letters numbers, and special characters.</div>"        ]
      },
      {
        "status":"focused",
        "how_to_use": "A focused state communicates when a user has highlighted an element using a keyboard or currently interacting with the field.",
        "code": [
          "<label class='d-block'><strong>Password</strong></label>",
          "<input type='password' autofocus>",
          "<div class='password-strength-meter'><div class='meter'></div><div class='meter'></div><div class='meter'></div><div class='meter-text'></div></div>",
          "<div class='password-criteria-text'>Your password must be 8-20 characters long, contain letters numbers, and special characters.</div>"        ]
      },
      {
        "status":"weak",
        "how_to_use": "Give the user immediate feedback on the validity and strength of a new password while it is being typed. Color red for unacceptable or password strength is weak.",
        "code": [
          "<label class='d-block'><strong>Password</strong></label>",
          "<input type='password'>",
          "<div class='password-strength-meter visible weak'><div class='meter'></div><div class='meter'></div><div class='meter'></div><div class='meter-text'></div></div>",
          "<div class='password-criteria-text'>Your password must be 8-20 characters long, contain letters numbers, and special characters.</div>"        ]
      },
      {
        "status":"medium",
        "how_to_use": "Give the user immediate feedback on the validity and strength of a new password while it is being typed. Color yellow for fair or password strength is medium.",
        "code": [
          "<label class='d-block'><strong>Password</strong></label>",
          "<input type='password'>",
          "<div class='password-strength-meter visible medium'><div class='meter'></div><div class='meter'></div><div class='meter'></div><div class='meter-text'></div></div>",
          "<div class='password-criteria-text'>Your password must be 8-20 characters long, contain letters numbers, and special characters.</div>"        ]
      },
      {
        "status":"strong",
        "how_to_use": "Give the user immediate feedback on the validity and strength of a new password while it is being typed. Color green for valid or password strength is strong.",
        "code": [
          "<label class='d-block'><strong>Password</strong></label>",
          "<input type='password'>",
          "<div class='password-strength-meter visible strong'><div class='meter'></div><div class='meter'></div><div class='meter'></div><div class='meter-text'></div></div>",
          "<div class='password-criteria-text'>Your password must be 8-20 characters long, contain letters numbers, and special characters.</div>"        ]
      },
      {
        "status":"disabled",
        "how_to_use": "This prevents the user from interacting with a field until certain conditions are met. When possible, show an alert or error message to inform users why the element is disabled.",
        "code": [
          "<label class='d-block disabled'><strong>Password</strong></label>",
          "<input type='password' disabled>",
          "<div class='password-strength-meter'><div class='meter'></div><div class='meter'></div><div class='meter'></div><div class='meter-text'></div></div>",
          "<div class='password-criteria-text'>Your password must be 8-20 characters long, contain letters numbers, and special characters.</div>"        ]
      },
      {
        "status":"error",
        "how_to_use": "Indicates there was an error with the field on submission. Only affected fields should be highlighted in red.",
        "code": [
          "<label class='d-block'><strong>Password</strong></label>",
          "<input type='password' class='error'>",
          "<div class='password-strength-meter'><div class='meter'></div><div class='meter'></div><div class='meter'></div><div class='meter-text'></div></div>",
          "<div class='password-criteria-text d-none'>Your password must be 8-20 characters long, contain letters numbers, and special characters.</div>",
          "<div class='error-msg'>Username and password don’t match.</div>"
        ]
      }
    ]
  },
  {
    "type": "range",
    "states": [
      {
        "status":"default",
        "how_to_use": "This is normal state of the element.",
        "code": [
          "<label class='d-block margin-bottom-s'><strong>Range</strong></label>",
          "<div class='slider-container'><span class='bar'><span class='fill'></span></span>",
          "<input class='js-form-element-state slider' id='slider' type='range' value='1' min='1' max='100' /></div>"
        ]
      },
      {
        "status":"focused",
        "how_to_use": "A focused state communicates when a user has highlighted an element using a keyboard or currently interacting with the field.",
        "code": [
          "<label class='d-block margin-bottom-s'><strong>Range</strong></label>",
          "<div class='slider-container'><span class='bar'><span class='fill'></span></span>",
          "<input class='js-form-element-state slider' id='slider' autofocus type='range' value='1' min='1' max='100' /></div>"
        ]
      },
      {
        "status":"disabled",
        "how_to_use": "This prevents the user from interacting with a field until certain conditions are met. When possible, show an alert or error message to inform users why the element is disabled.",
        "code": [
          "<label class='d-block disabled margin-bottom-s'><strong>Range</strong></label>",
          "<div class='slider-container disabled'><span class='bar'><span class='fill'></span></span>",
          "<input class='js-form-element-state slider' disabled id='slider' type='range' value='1' min='1' max='100' /></div>"
        ]
      },
      {
        "status":"error",
        "how_to_use": "Indicates there was an error with the field on submission. Only affected fields should be highlighted with error message.",
        "code": [
          "<label class='d-block margin-bottom-s'><strong>Range</strong></label>",
          "<div class='slider-container'><span class='bar'><span class='fill'></span></span>",
          "<input class='js-form-element-state slider' id='slider' type='range' value='1' min='1' max='100' /></div>",
          "<div class='error-msg'>Range cannot be 0.</div>"
        ]
      },
      {
        "status":"optional",
        "how_to_use": "Use (optional) to indicate non-mandatory fields rather than indicating only mandatory fields. This practice has proved to reduce stress in users when filling out long forms.",
        "code": [
          "<label class='d-block margin-bottom-s'><strong>Range</strong><span class='optional-label'> (optional)</span></label>",
          "<div class='slider-container'><span class='bar'><span class='fill'></span></span>",
          "<input class='js-form-element-state slider' id='slider' type='range' value='1' min='1' max='100' /></div>"
        ]
      }
    ]
  },
  {
    "type": "file",
    "states": [
      {
        "status":"default",
        "how_to_use": "This is normal state of the element.",
        "code": [
          "<label class='d-block margin-bottom-s'><strong>Choose file</strong></label>",
         "<input class='js-form-element-state' id='upload-file' type='file' name='file' />",
          "<label class='btn btn-secondary btn-sm' for='upload-file'>Browse</label>"        ]
      },
      {
        "status":"focused",
        "how_to_use": "A focused state communicates when a user has highlighted an element using a keyboard or currently interacting with the field.",
        "code": [
          "<label class='d-block margin-bottom-s'><strong>Choose file</strong></label>",
         "<input class='js-form-element-state' id='upload-file' autofocus type='file' name='file' />",
          "<label class='btn btn-secondary btn-sm' for='upload-file'>Browse</label>"        ]
      },
      {
        "status":"disabled",
        "how_to_use": "This prevents the user from interacting with a field until certain conditions are met. When possible, show an alert or error message to inform users why the element is disabled.",
        "code": [
          "<label class='d-block margin-bottom-s disabled'><strong>Choose file</strong></label>",
          "<input class='js-form-element-state' disabled id='upload-file' type='file' name='file' />",
          "<label class='btn btn-secondary btn-sm disabled' for='upload-file'>Browse</label>"        ]
      },
      {
        "status":"error",
        "how_to_use": "Use it to indicates there was an error with the field on submission. Only affected fields should be highlighted in red.",
        "code": [
          "<label class='d-block margin-bottom-s'><strong>Choose file</strong></label>",
          "<input class='js-form-element-state error' id='upload-file' type='file' name='file' />",
          "<label class='btn btn-secondary btn-sm' for='upload-file'>Browse</label>",
          "<div class='error-msg'>The file format is not supported.</div>"
        ]
      },
      {
        "status":"approved-field",
        "how_to_use": "Indicates that required field has a valid formate and content.",
        "code": [
          "<label class='d-block margin-bottom-s'><strong>Choose file</strong></label>",
          "<input class='js-form-element-state approved' id='upload-file' type='file' name='file' />",
          "<label class='btn btn-secondary btn-sm' for='upload-file'>Browse</label>"        ]
      },
      {
        "status":"optional",
        "how_to_use": "Use (optional) to indicate non-mandatory fields rather than indicating only mandatory fields. This practice has proved to reduce stress in users when filling out long forms.",
        "code": [
          "<label class='d-block margin-bottom-s'><strong>Choose file</strong><span class='optional-label'> (optional)</span></label>",
          "<input class='js-form-element-state' id='upload-file' type='file' name='file' />",
          "<label class='btn btn-secondary btn-sm' for='upload-file'>Browse</label>"        ]
      }
    ]
  }
]
