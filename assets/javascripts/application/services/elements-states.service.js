const getElementsStatesService = (callback) => { // eslint-disable-line no-unused-vars
  $.when(
    $.ajax({
      url: `/javascripts/application/models/element-states.json`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.log(`there was an error retrieving the element states data`)
        callback()
      }
    }))
    .then(function (data) {
      callback(data)
    })
}
